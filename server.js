
var pg = require('pg');
var restify = require('restify');



var server = restify.createServer();
server.use(restify.queryParser()); // see Query Parser documentation from http://mcavage.me/node-restify/
server.use(restify.bodyParser());  // see Body Parser documentation from http://mcavage.me/node-restify/

// simple logging function to log everything to the console
server.use(function logger(req,res,next) {
  console.log(new Date(),req.method,req.url);
  next();
});
server.use(restify.CORS()); // Allow Cross-origin resource sharing - basically accept requests outside from this domain
server.use(restify.fullResponse()); // Also for CORS



// Server routes defined below

// simple hello route
server.get('/hello/:name', function(req, res, next){

	// respond with simple string
	res.send('hello ' + req.params.name); // notice how you can get access to the URL paramaters through the req.params 
	next(); // pass the control to the next handler in the chain - read more for example here: http://stackoverflow.com/questions/22354850/the-consequences-of-not-calling-next-in-restify
});


// Route which demonstrates how to make Postgresql query in Heroku
server.get('/dbdemo', function(req,res,next){

	// Connect to the database
	pg.connect(process.env.DATABASE_URL, function(err, client, done){
		if (err) throw err;
  		console.log('Connected to postgres! Getting schemas...');

		// Make the query
		client.query('SELECT * FROM example', function(err, result){
			done();
			res.send(result.rows);
			next();
		});
	});
});

// Route which demonstrates how to post data to restify and then use it to create a new record in DB
server.post('/dbdemo', function(req, res, next){
	// Connect to the database
	pg.connect(process.env.DATABASE_URL, function(err, client, done){
		if (err) throw err;

		// Make the query
		client.query("INSERT INTO example (\"message\", \"user\") VALUES ('" + req.body.message + "', '" + req.body.user + "')", function(err, result){
			done();		
			if(err == null)
			{
				res.send(200);
			}
			else
			{
				console.log(err);
				res.send(400);	
			}
					
			next();
		});
	});
});



var port = process.env.PORT || 8080;
server.listen(port, function() {
  console.log('%s listening at %s', server.name, server.url);
});